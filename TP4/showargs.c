#include<stdio.h>

// argc : arg count (nb d'argument, toujours au moins 1)
// argv : tableau de argc pointeur vers char (les arguments,
//  nom du programme inclu)
int main(int argc, char *argv[]) {
    for (int i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    return 0;
}


