#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    long double resultat = 0;

    for (int i = 1; i < argc; i++) {
        resultat += strtold(argv[i], NULL);
    }
    
    // note : (man 3 printf)
    // %f  -> float
    // %lf -> double
    // %Lf -> long double 
    printf("%.2Lf\n", resultat);

    exit(EXIT_SUCCESS);
}

