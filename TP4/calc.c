#include<stdio.h>
#include<stdlib.h>
#include<string.h>

// double au lieu de float (plus précis)

double add(long double x, long double y) {
    return x + y;
}

double sub(long double x, long double y) {
    return x - y;
}

double mul(long double x, long double y) {
    return x * y;
}

// divv pour ne pas avoir de conflit avec div 
// défini dans stdlib.h
double divv(long double x, long double y) {
    return x / y;
}

double usage(char *myname) {
    printf("Usage: %s [+-*/] val1 val2\n", myname);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    long double a,b,res;
    char op;

    // %f pour un float, %lf pour un double, %LF pour un long double

    if (argc != 4) {
        usage(argv[0]); // existe toujours : mon nom
    }

    if (strlen(argv[1]) != 1) { // existe car argv est 4 (> 1)
        usage(argv[0]); 
    }

    op = argv[1][0]; // premier (seul) caractère de argv[1]
    // strtold (man 3 strtold) convertit chaîne en long double
    a = strtold(argv[2], NULL); // existe (argv est 4)
    b = strtold(argv[3], NULL); // existe (argv est 4)

    switch (op) {
        case '+':
           res = add(a, b);
           break;
        case '-':
           res = sub(a, b);
           break;
        case '*':
           res = mul(a, b);
           break;
        case '/':
           res = divv(a, b);
           break;
        default:
           usage(argv[0]);
    } 
    printf("%.2Lf\n", res);
    return 0; // ou exit(EXIT_SUCCESS);
}



