#include <stdio.h>

int main(){
	float nombre1, nombre2;
	char operation;
	
	printf("netrez le premier nombre:");
	scanf("%f", &nombre1);
	
	printf("entrez le deuxième nombre:");
	scanf("%f", &nombre2);
	
	printf("entrez l'opé a réaliser(+ ,- ,* ,/ ): ");
	scanf ("%c", &operation);
	
	switch (operation){
		case'+':
			printf ("le resultat est : %2f\n",nombre1+nombre2);
			break;
		case '-':
			printf ("le résultat est: %2f\n",nombre1-nombre2);
			break;
		case '*':
			printf ("le résultat est %2f\n", nombre1*nombre2);
			break;
		case '/':
			if (nombre2 !=0)
			printf("le resultat est %2f\n",nombre1/nombre2);
			else 
			printf("erreur: div par zéro impossible.\n");
			break;
		default: 
			printf("opé invalide.\n");
			break;
		}
	return 0;
}
