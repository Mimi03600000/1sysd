#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    char nom[50];
    int nombre_secret, nombre_tentatives = 0, guess;

    printf("Entrez votre nom : ");
    scanf("%s", nom);

    printf("Bonjour, %s ! Bienvenue dans le jeu de devinette.\n", nom);

    srand(time(NULL));

    nombre_secret = rand() % 100 + 1;

    do {
        printf("Devinez le nombre entre 1 et 100 : ");
        scanf("%d", &guess);
        nombre_tentatives++;

        if (guess > nombre_secret) {
            printf("Le nombre secret est plus petit.\n");
        } else if (guess < nombre_secret) {
            printf("Le nombre secret est plus grand.\n");
        } else {
            printf("Félicitations, %s ! Vous avez deviné le nombre secret en %d tentatives.\n", nom, nombre_tentatives);
        }
    } while (guess != nombre_secret);

    return 0;
}

