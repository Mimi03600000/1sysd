#include<stdio.h>

int main() {

    char msg[] = "Hello World!"; 
    char *p;
    int i, l;
    
    i = 0;
    while (msg[i] != '\0') { // ou 0 mais surtout pas '0' (89)
        printf("%c\n", msg[i]);
        i++;
    }

    p = msg;
    while (*p) { // tant que le caractère vers lequel pointe p 
                 // n'est pas 0.
        printf("%c\n", *p);
        p++;
    }

    // calcul de la longueur d'un chaîne
    l = 0;
    p = msg;
    while (*p++) {
        l++;
    }
    printf("Longueur de la chaîne : %d\n", l);

}


