#include<stdio.h>
#include<stdlib.h>

int slen(char *s) {
    int l = 0;
    char *p;
    p = s;
    while (*p++) {
        l++;
    }
    return l;
}

int is_upper(char *s) {
    int upper = -1;
    // on passe à faux dès qu'on trouve une minuscule
    // du coup une chaîne sans lettre du tout est 
    // considérée comme toute en majuscule, ça se
    // défend (différent de isupper() en Python)
    while (*s && upper) { // le && upper rend le break inutile
        if (*s >= 'a' && *s <= 'z') { // minuscule
            upper = 0;
            //break;
        } 
        s++;
    }
    return upper;
}


// TODO: cf. transparents du cours.

// trouver condition et expression
// ( rappel : && (et) || (ou) ! (non) )
int sequal(char *s1, char *s2) {
    char *p, *q;
    p = s1;
    q = s2;

    while (condition) {
        p++;
        q++;
    }
    return expression;
}


int main() {
    char s1[] = "Hello World!";
    char s2[] = "Bonjour";
    char s3[] = "Bonjour";
    char s4[] = "Bonsoir";
    char s5[] = "Bon";

    printf("Longeur de \"%s\" : %d\n", s1, slen(s1));
    
    if (sequal(s1, s2)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }
    if (sequal(s2, s3)) {
        printf("s2 et s3 sont égales\n");
    } else {
        printf("s2 et s3 sont différentes\n");
    }
    if (sequal(s2, s4)) {
        printf("s2 et s4 sont égales\n");
    } else {
        printf("s2 et s4 sont différentes\n");
    }
    if (sequal(s2, s5)) {
        printf("s2 et s5 sont égales\n");
    } else {
        printf("s2 et s5 sont différentes\n");
    }
    
    return 0;
}

