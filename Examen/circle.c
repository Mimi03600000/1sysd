#include <stdio.h>
#include <stdlib.h>

#define pi 3.14159

double perimetre(double rayon) {
    return 2 * pi * rayon;
}

double aire(double rayon) {
    return pi * rayon * rayon;
}

int main() {
    double rayon;

    printf("Valeur du rayon: ");
    scanf("%lf", &rayon);

    printf("Le périmètre est de: %.2f\n", perimetre(rayon));
    printf("L'aire est de: %.2f\n", aire(rayon));

    return 0;
}

