#include <stdio.h>
#include <stdlib.h>

long long int factorial(int n) {
    if (n == 0) {
        return 1;
    } else {
        return n * factorial(n-1);
    }
}

int main(int argc, char *argv[]) {
    if (argc!= 3) {
        printf("Usage:./factorial <début> <fin>\n");
        return 1;
    }

    int start = atoi(argv[1]);
    int end = atoi(argv[2]);

    for (int i = start; i <= end; i++) {
        printf("%d! = %lld\n", i, factorial(i));
    }

    return 0;
}
