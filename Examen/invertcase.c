#include <stdio.h>
#include <ctype.h>


void invertcase(char *s)
{
    for(;*s != '\0';s++) {
        if (isalpha(*s)) {*s = (*s == toupper(*s)) ? tolower(*s) : toupper(*s);}
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: ./invertcase <string>\n");
        return 1;
    }

    invertcase(argv[1]);
    printf("%s\n", argv[1]);

    return 0;
}
